<?php get_header(); ?>


<body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top"><?php bloginfo('name'); ?></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#team">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
        <div class="container">
            <div class="intro-text">
                <?php
                $page = get_page_by_path("welcome");
                ?>
                <div class="intro-lead-in"><?php echo $page->post_title; ?></div>
                <div class="intro-heading text-uppercase"><?php echo $page->post_content; ?></div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">
                    Tell Me More
                </a>
            </div>
        </div>
    </header>

    <!-- About -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                    <?php
                    $page = get_page_by_title("About");
                    ?>
                    <h2 class="section-heading text-uppercase"><?php echo $page->post_title; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $page->post_content; ?></h3>
                </div>
            </div>
        </div>
    </section>


    <!-- Services -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">


                    <?php
                    $category = get_category_by_slug("services");
                    ?>
                    <h2 class="section-heading text-uppercase"><?php echo $category->name; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $category->description; ?></h3>
                </div>
            </div>
            <?php
            $service_items = get_posts(array("category" => $category->term_id));
            ?>

            <div class="row text-center">
                <?php foreach ($service_items as $service): ?>

                    <div class="col-md-4">
    <!--                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                        </span>-->

                        <?php
                        $media = get_attached_media('image', $service);
                        reset($media);
                        $image_path = $media[key($media)]->guid;

                        $item_content = $service->post_content;
                        $item_content = preg_replace("/<img[^>]+\>/i", " ", $item_content);
                        $item_content = apply_filters('the_content', $item_content);
                        $item_content = str_replace(']]>', ']]>', $item_content);
                        ?>

                        <!--<span class="fa-stack fa-5x">-->
                        <span>
                            <img src="<?php echo $image_path ?>" class="img-fluid" /> 
                            <!--height="140" width="140" style="margin-bottom: 40px"/>-->
                        </span>
                        <h4 class="service-heading"><?php echo $service->post_title; ?></h4>
                        <div class="section-subheading text-muted" style="text-justify: auto !important">
                            <?php echo $item_content; ?> 
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <!-- Portfolio Grid -->
    <section class="bg-light" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                    <?php
                    $category_portfolio = get_category_by_slug("portfolio");
                    ?>
                    <h2 class="section-heading text-uppercase"><?php echo $category_portfolio->name; ?></h2>
                    <h3 class="section-subheading text-muted"><?php echo $category_portfolio->description; ?></h3>
                </div>
            </div>
            <div class="row">

                <?php
                $portfolio_items = get_posts(array("category" => $category_portfolio->term_id));

                foreach ($portfolio_items as $portfolio):
                    ?>

                    <div class="col-md-4 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#<?php echo $portfolio->post_name; ?>">
                            <div class="portfolio-hover">
                                <div class="portfolio-hover-content">
                                    <i class="fa fa-plus fa-3x"></i>
                                </div>
                            </div>

                            <?php
                            $media = get_attached_media('image', $portfolio);
                            reset($media);
                            $image_path = $media[key($media)]->guid;

                            $item_content = $portfolio->post_content;
                            $item_content = preg_replace("/<img[^>]+\>/i", " ", $item_content);
                            $item_content = apply_filters('the_content', $item_content);
                            $item_content = str_replace(']]>', ']]>', $item_content);
                            ?>

                            <img class="img-fluid" src="<?php echo $image_path ?>" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4><?php echo $portfolio->post_title; ?></h4>
                            <div class="section-subheading text-muted"><?php echo $item_content; ?></div>
                        </div>
                    </div>


                    <!----modal dialogs-->
                    <div class="portfolio-modal modal fade" id="<?php echo $portfolio->post_name; ?>" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1000">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="close-modal" data-dismiss="modal">
                                    <div class="lr">
                                        <div class="rl"></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-8 mx-auto">
                                            <div class="modal-body">
                                                <!-- Project Details Go Here -->
                                                <h2 class="text-uppercase"><?php echo $portfolio->post_name; ?></h2>
                                                <!--<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>-->
                                                <img class="img-fluid d-block mx-auto" src="<?php echo $image_path ?>" alt="">
                                                <?php echo $item_content; ?>
                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                    <i class="fa fa-times"></i>
                                                    Close Project</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </section>


    <!-- Team -->
    <section class="bg-light" id="team">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                    <?php
                    $category_team = get_category_by_slug("team");
                    ?>


                    <h2 class="section-heading text-uppercase"><?php echo $category_team->name; ?></h2>
                    <!--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>-->
                </div>
            </div>

            <div class="row">
                <?php
                $team_items = get_posts(array("category" => $category_team->term_id));

                foreach ($team_items as $team_item):

                    $media = get_attached_media('image', $team_item);
                    reset($media);
                    $image_path = $media[key($media)]->guid;

                    $item_content = $team_item->post_content;
                    $item_content = preg_replace("/<img[^>]+\>/i", " ", $item_content);
                    $item_content = apply_filters('the_content', $item_content);
                    $item_content = str_replace(']]>', ']]>', $item_content);
                    ?>
                    <div class="col-sm-4">
                        <div class="team-member">
                            <img class="mx-auto rounded-circle" src="<?php echo $image_path ?>" alt="">
                            <h4><?php echo $team_item->post_title ?></h4>
                            <p class="text-muted"><?php echo $item_content; ?></p>
                            <ul class="list-inline social-buttons">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <div class="large text-muted">
                        <?php echo $category_team->description; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Clients -->
<!--    <section class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img class="img-fluid d-block mx-auto" src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/logos/envato.jpg" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img class="img-fluid d-block mx-auto" src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/logos/designmodo.jpg" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img class="img-fluid d-block mx-auto" src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/logos/themeforest.jpg" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img class="img-fluid d-block mx-auto" src="<?php echo get_bloginfo('template_directory'); ?>/assets/img/logos/creative-market.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>-->

    <!-- Contact -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Contact Us</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form id="contactForm" name="sentMessage" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" id="name" type="text" placeholder="Your Name *" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="email" type="email" placeholder="Your Email *" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" id="message" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; <?php echo bloginfo('name'); ?> <?php echo date("Y"); ?></span>
                </div>

                <div class="col-md-4"> 

                    <p>
                        <a href="#">Back to top</a>
                    </p>

                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->

    <?php get_footer(); ?>

</body>
</html>