<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- Bootstrap core JavaScript -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo get_bloginfo('template_directory'); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/assets/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo get_bloginfo('template_directory'); ?>/assets/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/assets/js/agency.min.js"></script>
<?php wp_footer(); ?> 

